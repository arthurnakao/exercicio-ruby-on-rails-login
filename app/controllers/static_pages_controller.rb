class StaticPagesController < ApplicationController
    before_action :impede_logado, only: [:home]

    def home
    end


    private
    def impede_logado
        if logged_in?
            redirect_to current_user
        end
    end
end
