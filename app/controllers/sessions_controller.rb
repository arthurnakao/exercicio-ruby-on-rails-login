class SessionsController < ApplicationController
    before_action :impede_logado, only: [:new]
    before_action :impede_nao_logado, only: [:destroy]

    def new

    end

    def create
        user = User.find_by(email: params[:session][:email])
        if user && user.authenticate(params[:session][:password])
            log_in(user)
            redirect_to user
        else
            render 'new'
        end
    end

    def destroy
        log_out
        redirect_to root_path
    end

    private
    def impede_nao_logado
        if !logged_in?
            redirect_to login_path
        end
    end

    def impede_logado
        if logged_in?
            redirect_to current_user
        end
    end
end
